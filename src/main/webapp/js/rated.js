$(document).ready(function () {
    /*
     * MATERIAZLIZE
     */
    $('ul.tabs').tabs('select_tab', 'test4');
    $('.chips').material_chip();
    $('.chips-initial').material_chip({
        data: [{tag: 'Apple'}, {tag: 'Microsoft'}, {tag: 'Google'}]
    });
    $('.chips-placeholder').material_chip({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag'
    });
    $('.chips').on('chip.add', function (e, chip) {
        alert("on add");
        alert(chip.tag);
        $('.chips-initial').material_chip('data').forEach(function (untag) {
            alert(untag.tag);
        });
    });

    $('.chips').on('chip.delete', function (e, chip) {
        alert("on delete");
    });

    $('.chips').on('chip.select', function (e, chip) {
        alert("on select");
    });
    $(".button-collapse").sideNav();
    $('.modal-trigger').leanModal();
    /*
     * END MATERIALIZE
     */


    var ratedFilms = $("#peliculesValorades");
    var servletURL = "rated?action=ratedFilmList";
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: "json",
        async: true,
        url: servletURL,
        success: function (data) {
            var myHtml = renderListFilms(data);
            ratedFilms.html(myHtml);
            var dades = millorPelicula(data);
            $("#bestfilm-rate").text(dades[0]);
            $("#bestfilm-med").text(dades[1]);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.info('in error');
            console.log(jqXHR, textStatus, errorThrown);
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
});
$(document).on('click', '[class*="deleteFilm"]', function () {
    var film = $(this).attr("id");

    var servletURL = "rated?action=deleteRatedFilm&film=" + film;
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: "json",
        async: true,
        url: servletURL,
        success: function (data) {
            window.location.href = "valoracions.html";

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.info('in error');
            console.log(jqXHR, textStatus, errorThrown);
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
});



function renderListFilms(data) {
    var myHtml = "";
    $.each(data.jsonArray, function (index) {
        myHtml += '<div class="col s12 m12 l12"> <div class="card grey lighten-4 hoverable">';
        myHtml += renderFilm(data.jsonArray[index]);
        myHtml += '</div></div>';
    });
    return myHtml;
}

function renderFilm(dataFilm) {
    //TODO si stock és 0
    var myHtmlP = "";
    var film = "";
    var med = 0.0;
    var rate = 0;
    var rateList;
    $.each(dataFilm, function (key, value) {
        if (key == 'name') {
            film = value;
        }
        if (key == 'med') {
            med = parseFloat(value);
        }
        if (key == 'rate') {
            rate = parseInt(value);
        }
        if (key == 'rateList') {
            rateList = value;
        }
    });
 
    myHtmlP +='<div class="card-panel grey lighten-5 z-depth-1 hoverable"><div class="row valign-wrapper">';
    myHtmlP += '<div class="col s2"><img  class="circle responsive-img" src="img/' + film + '.png"/></div>';
    myHtmlP += '<div class="col s10">\n\
                <div class="chip"><h6>Pel·lícula: <span id="name-' + film + '">' + film + '</h6></div>';
    myHtmlP += '<div class="chip"><h6>Mitjana: <span id="med-' + film + '">' + med + '</h6></div>';
    myHtmlP += '<div class="chip"><h6>Última Valoració: <span id="rate-' + film + '">' + rate + '</span></h6></div> ';
    myHtmlP += '<div class="chip"><h6>Totes: <span id="list-' + film + '">' + rateList + '</span></h6></div></div>';
    myHtmlP += '<div class="card-action right-align"><a class ="deleteFilm" href="#" id="' + film + '">Eliminar</a>';
    myHtmlP += '</div></div></div>';
    return myHtmlP;
}

function millorPelicula(data){
    var dades = ["",0.0];
     $.each(data.jsonArray, function (index) {
        
        var film = data.jsonArray[index];
        var m = film["med"];
        if (m > dades[1]){
            dades[0] = film["name"];
            dades[1] = parseFloat(m);
        }
     });
    
  
    return dades;
}


