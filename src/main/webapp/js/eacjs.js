$(document).ready(function () {
    /*
     * MATERIAZLIZE
     */
    $('ul.tabs').tabs('select_tab', 'test4');
    $('.chips').material_chip();
    $('.chips-initial').material_chip({
        data: [{tag: 'Apple'}, {tag: 'Microsoft'}, {tag: 'Google'}]
    });
    $('.chips-placeholder').material_chip({
        placeholder: 'Enter a tag',
        secondaryPlaceholder: '+Tag'
    });
    $('.chips').on('chip.add', function (e, chip) {
        alert("on add");
        alert(chip.tag);
        $('.chips-initial').material_chip('data').forEach(function (untag) {
            alert(untag.tag);
        });
    });

    $('.chips').on('chip.delete', function (e, chip) {
        alert("on delete");
    });

    $('.chips').on('chip.select', function (e, chip) {
        alert("on select");
    });
    $(".button-collapse").sideNav();
    $('.modal-trigger').leanModal();
    /*
     * END MATERIALIZE
     */


    var pelicules = $("#pelicules");
    var servletURL = "Cartell?action=listFilms";
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: "json",
        async: true,
        url: servletURL,
        success: function (data) {
            var myHtml = renderListProducts(data);
            pelicules.html(myHtml);
            var dades = millorPelicula(data)
            $("#bestfilm-rate").text(dades[0]);
            $("#bestfilm-med").text(dades[1]);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.info('in error');
            console.log(jqXHR, textStatus, errorThrown);
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });

  //p3//
    var servletURL = "user?action=formUser";
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: "json",
        async: true,
        url: servletURL,
        success: function (data) {
            $("#username").html("<h5>Valoracions de l'usuari: " + data.user +'</h5>');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.info('in error');
            console.log(jqXHR, textStatus, errorThrown);
        }
    });


});
$(document).on('click', '[class*="rate"]', function () {
    var peliculaRate = $(this).attr("id").split("-");
    var pelicula = peliculaRate[0];
    var rate = peliculaRate[1];
    var afegir = $(this.parentElement);
    var servletURL = "Cartell?action=addRateFilm&pelicula=" + pelicula + "&valoracio=" + rate;
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: "json",
        async: true,
        url: servletURL,
        success: function (data) {
            if (data.mitjanaPelicula == -1.0) {
                $("#mitjana-" + data.peliculaValorada).text('0');
                alert("Sense mitjana. Només una valoració");
            } else {
                afegir.hide();
                $("#check-" + pelicula).show();
                $("#mitjana-" + data.peliculaValorada).text(data.mitjanaPelicula);
                $("#rate-" + data.peliculaValorada).text(rate);
                
                var bestFilmMed = parseFloat($("#bestfilm-med").text());
                if(data.mitjanaPelicula > bestFilmMed){
                    $("#bestfilm-rate").text(data.peliculaValorada);
                    $("#bestfilm-med").text(data.mitjanaPelicula);
                }                
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.info('in error');
            console.log(jqXHR, textStatus, errorThrown);
            alert("You can not send Cross Domain AJAX requests: " + errorThrown);
        }
    });
});



function renderListProducts(data) {
    var myHtml = "";
    $.each(data.jsonArray, function (index) {
        myHtml += '<div class="col s12 m3 l3"> <div class="card grey lighten-4 hoverable">';
        myHtml += renderFilm(data.jsonArray[index]);
        myHtml += '</div></div>';
    });
    return myHtml;
}

function renderFilm(dataFilm) {
    var myHtmlP = "";
    var pelicula = "";
    var mitjana = 0.0;
    var valoracio = 0;
    var quantity = 0;
    var med = 0.0
    var rated = false;
    $.each(dataFilm, function (key, value) {
        if (key == 'name') {
            pelicula = value;
        }
        if (key == 'valoracio') {
            valoracio = parseInt(value);
        }
        if (key == 'mitjana') {
            mitjana = parseFloat(value);
        }
        if (key == 'rated') {
            if (value == 'SI') {
                rated = true;
            } else {
                rated = false;
            }
        }
    });
    myHtmlP += '<div class="card-image"><img src="img/' + pelicula + '.png"/><span class="card-title">' + pelicula + '</span></div>';
    myHtmlP += '<div class="card-content"><div class="chip"><h6>Valoració: <span id="rate-' + pelicula + '">' + valoracio + '</h6></div>';
    myHtmlP += '<div class="chip"><h6>Mitjana: <span id="mitjana-' + pelicula + '">' + mitjana + '</span></h6></div></div>';
    
    if (rated) {
        myHtmlP += '<div class="card-action right-align">';
        myHtmlP += '<img id ="check-' + pelicula + '" style="width: 10px;" src="img/check.png"/></div>';

    } else {
        myHtmlP += '<div class="card-action right-align"><div>';
       for (var i = 1; i <= 5; i++){
        myHtmlP += '<a class ="rate" href="#" id="' + pelicula + '-' +i+'">'+i+'</a>';
       }
        myHtmlP += '</div><img id ="check-' + pelicula + '" style="display: none;width: 10px;" src="img/check.png"/></div>';
    }

    return myHtmlP;
}


function millorPelicula(data){
    var dades = ["",0.0];
     $.each(data.jsonArray, function (index) {
        
        var film = data.jsonArray[index];
        var m = film["mitjana"];
        if (m > dades[1]){
            dades[0] = film["name"];
            dades[1] = parseFloat(m);
        }
     });
    
  
    return dades;
}