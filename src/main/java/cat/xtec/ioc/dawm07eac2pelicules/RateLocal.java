package cat.xtec.ioc.dawm07eac2pelicules;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@Local
public interface RateLocal {

    public List<Pelicula> getRatedFilms();

    public void setRatedFilms(List<Pelicula> ratedFilms);
}
