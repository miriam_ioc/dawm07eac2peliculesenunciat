package cat.xtec.ioc.dawm07eac2pelicules;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@WebServlet(name = "RatedServlet", urlPatterns = {"/rated"})
public class RatedServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "ratedFilmList":
                ratedFilmList(request, response);
                break;
            case "deleteRatedFilm":
                deleteRatedFilm(request, response);
                break;
        }
    }

    private void ratedFilmList(HttpServletRequest request, HttpServletResponse response) {//No funciona
        try (PrintWriter out = response.getWriter()) {
            RateLocal ratebean = (RateLocal) request.getSession().getAttribute("ratebean");
            if (ratebean == null) {
                ratebean = (RateLocal) new InitialContext().lookup("java:global/dawm07eac2Pelicules/Rate");
                request.getSession().setAttribute("ratebean", ratebean);
                ratebean.setRatedFilms(new ArrayList<Pelicula>());
                
            }
            JSONObject json = new JSONObject();
            JSONArray array = new JSONArray();
            
            for (Pelicula p : ratebean.getRatedFilms()) {
                LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
                jsonOrderedMap.put("name", p.getName());
                jsonOrderedMap.put("rate", p.getValoracio().toString());
                jsonOrderedMap.put("med", p.getMitjana().toString());
                jsonOrderedMap.put("rateList", p.getTotesValoracions().toString());

                JSONObject member = new JSONObject(jsonOrderedMap);
                array.put(member);
            }
            json.put("jsonArray", array);
            out.print(json.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteRatedFilm(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            RateLocal ratebean = (RateLocal) request.getSession().getAttribute("ratebean");
            String peliculaParam = request.getParameter("film");
            for (Pelicula p : ratebean.getRatedFilms()) {
                if (p.getName().equals(peliculaParam)) {
                    p.eliminaUltimaValoracio();
                    ratebean.getRatedFilms().remove(p);
                }

            }
            String resposta = "OK";
            JSONObject json = new JSONObject();
            json.put("resposta", resposta);
            out.print(json.toString());
        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
