/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.xtec.ioc.dawm07eac2pelicules;

import javax.ejb.Local;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@Local
public interface ValidateFilmBeanLocal {

    public Boolean isValidFileImageName(String filmName, String fileImageName);
}
