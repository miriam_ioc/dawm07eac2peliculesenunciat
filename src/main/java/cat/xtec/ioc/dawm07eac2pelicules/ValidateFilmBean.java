/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.xtec.ioc.dawm07eac2pelicules;

import javax.ejb.Stateless;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@Stateless
public class ValidateFilmBean implements ValidateFilmBeanLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public Boolean isValidFileImageName(String filmName, String fileImageName) {
        boolean validacio = false;
        String[] nomPelicula = fileImageName.split("\\.");
        if (filmName.equals(nomPelicula[0])) {
            validacio = true;
        }
        return validacio;
    }
}
