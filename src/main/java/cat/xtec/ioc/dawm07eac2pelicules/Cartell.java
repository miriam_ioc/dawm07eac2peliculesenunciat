package cat.xtec.ioc.dawm07eac2pelicules;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.console;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@MultipartConfig(location = "/")
public class Cartell extends HttpServlet {

    @EJB
    private ValidateFilmBeanLocal validacio;
    private List<Pelicula> pelicules = new ArrayList<Pelicula>();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        Enumeration valorsInicials = getServletConfig().getInitParameterNames();
        do {
            String name = valorsInicials.nextElement().toString();
            String valorW = getServletConfig().getInitParameter(name);
            int valoracio = Integer.parseInt(valorW);
            pelicules.add(new Pelicula(name, valoracio));
        } while (valorsInicials.hasMoreElements());

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accio = request.getParameter("action");
        switch (accio) {
            case "listFilms":
                listFilms(request, response);
                break;
            case "addRateFilm":
                addRateFilm(request, response);
                break;
            case "createFilm":
                createFilm(request, response);
                break;
        }
    }

    protected void listFilms(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        try (PrintWriter out = response.getWriter()) {
            for (Pelicula p : pelicules) {

                LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
                jsonOrderedMap.put("name", p.getName());
                jsonOrderedMap.put("valoracio", p.getValoracio().toString());
                jsonOrderedMap.put("mitjana", p.getMitjana().toString());

                if (checkRatedFilmSession(request, p)) {
                    jsonOrderedMap.put("rated", "SI");
                } else {
                    jsonOrderedMap.put("rated", "NO");
                }

                JSONObject member = new JSONObject(jsonOrderedMap);
                array.put(member);
            }
            json.put("jsonArray", array);
            out.print(json.toString());
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }

    }

    protected void addRateFilm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String peliculaValorada = request.getParameter("pelicula");
        Integer valoracioDonada = Integer.parseInt(request.getParameter("valoracio"));
        try (PrintWriter out = response.getWriter()) {
            Double mitjana = 0.0;
            for (Pelicula p : pelicules) {
                if (p.getName().equals(peliculaValorada)) {
                    p.setValoracio(valoracioDonada);
                    mitjana = p.getMitjana();

                    addRatedFilmToSession(request, p);
                }
            }
            JSONObject json = new JSONObject();
            json.put("peliculaValorada", peliculaValorada);
            json.put("mitjanaPelicula", mitjana);
            out.print(json.toString());
            out.close();
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }
    }

    protected void createFilm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String rutaAplicacio = request.getServletContext().getRealPath("");
        String rutaImg = rutaAplicacio + "/img";
        try {
            Part p = request.getPart("fileImageName");
            if (isValidFileName(request.getParameter("fpname"), p.getSubmittedFileName())) {
                File nouArxiu = new File(rutaImg, p.getSubmittedFileName());
                InputStream input = p.getInputStream();
                Files.copy(input, nouArxiu.toPath(), REPLACE_EXISTING);
                Pelicula novaP = new Pelicula(request.getParameter("fpname"), 1);
                this.pelicules.add(novaP);
                response.sendRedirect("index.html");
            } else {
                response.sendRedirect("index.html");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private boolean isValidFileName(String paramName, String fileName) {
        boolean validat = false;
        validat = validacio.isValidFileImageName(paramName, fileName);
        if (validat == true) {
            for (Pelicula p : pelicules) {
                if (p.getName().equals(paramName)) {
                    validat = false; //trobada coincidencia de nom
                }
            }
        }
        return validat;
    }

    /*
     P3
     */
    private void addRatedFilmToSession(HttpServletRequest request, Pelicula film) {
        try {
            RateLocal rateBean = (RateLocal) request.getSession().getAttribute("ratebean");
            if (rateBean == null) {
                rateBean = (RateLocal) new InitialContext().lookup("java:global/dawm07eac2Pelicules/Rate");
                rateBean.setRatedFilms(new ArrayList<Pelicula>());
                request.getSession().setAttribute("ratebean", rateBean);
            }
            rateBean.getRatedFilms().add(film);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private Boolean checkRatedFilmSession(HttpServletRequest request, Pelicula film) {
        try {
            RateLocal rateBean = (RateLocal) request.getSession().getAttribute("ratebean");
            if (rateBean == null) {
                rateBean = (RateLocal) new InitialContext().lookup("java:global/dawm07eac2Pelicules/Rate");
                rateBean.setRatedFilms(new ArrayList<Pelicula>());
                request.getSession().setAttribute("ratebean", rateBean);
            }
            for (Pelicula p : rateBean.getRatedFilms()) {
                if (p.getName().equals(film.getName())) {
                    return true;
                }
            }
        } catch (Exception e) {

        }
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
