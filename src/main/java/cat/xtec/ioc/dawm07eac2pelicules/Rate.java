package cat.xtec.ioc.dawm07eac2pelicules;

import java.util.List;
import javax.ejb.Stateful;

/**
 *
 * @author EAC Miriam Diéguez Flores
 */
@Stateful
public class Rate implements RateLocal {

    private List<Pelicula> ratedFilms;

    @Override
    public List<Pelicula> getRatedFilms() {
        return ratedFilms;
    }

    @Override
    public void setRatedFilms(List<Pelicula> ratedFilms) {
        this.ratedFilms = ratedFilms;
    }
}
